const mysql = require('mysql')

const connection = mysql.createConnection({
  host: 'localhost',
  port: 3306,
  database: '<db>',
  user: '<username>',
  password: '<pass>'
})

module.exports = { connection }