const express = require('express');
const userRouter = require('./routes/user');
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use('/', userRouter);

app.listen(8001, () => console.log('App listening on port 8001...'))
module.exports = app;
