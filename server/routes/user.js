const express = require('express');
const router = express.Router();
const conn = require('../config').connection

const USERS = 'user', FRIENDS = 'userFriends'
const RESULTS_PER_PAGE = 3

router.get('/users', function (req, res) {
  try {
    let offset = req.query.page
    let query = `SELECT * FROM ${USERS}`
    if(offset !== undefined) {
      query += ` limit ${offset*RESULTS_PER_PAGE}, ${RESULTS_PER_PAGE}`
    }
    return conn.query(query, function (err, rows, fields) {
      if (err) throw new Error(err);
      res.json(rows);
    })
  }
  catch (err) {
    console.log(err)
    return []
  }
})

router.get('/friends', function (req, res) {
  try {
    if (!req.query.userId) throw new Error('Missing arguments.')
    let offset = req.query.page
    let userId = req.query.userId
    let query = `SELECT id, fname, lname from (
      (
        SELECT friend_id
        from ${FRIENDS}
        where user_id = ?
      ) as t1
      INNER JOIN
      (
        SELECT * from ${USERS}
      ) as t2
          ON t2.id = t1.friend_id
      )`

      if(offset !== undefined) {
        query += ` limit ${offset*RESULTS_PER_PAGE}, ${RESULTS_PER_PAGE}`
      }
      
      return conn.query(query, [ userId ], function (err, rows, fields) {
      if (err) throw err;
      res.json(rows)
    })
  }
  catch (err) {
    console.log(err)
    res.json({ error: true, message: err.message })
  }
})

router.get('/fof', function (req, res) {
  try {
    if (!req.query.userId) throw new Error('Missing arguments.')
    let offset = req.query.page
    let userId = req.query.userId
    let query = `SELECT id, fname, lname from (
        (
          SELECT uf2.friend_id as fof_id
          FROM ${FRIENDS} uf1
          JOIN ${FRIENDS} uf2 ON uf1.friend_id = uf2.user_id
          WHERE uf2.friend_id NOT IN (SELECT friend_id FROM ${FRIENDS} WHERE user_id = ?)
            AND uf1.user_id = ${userId}
        ) as t1
        INNER JOIN
        (
          select * from ${USERS}
          where id != ?
        ) as t2
            ON t2.id = t1.fof_id
      )`

      if(offset !== undefined) {
        query += ` limit ${offset*RESULTS_PER_PAGE}, ${RESULTS_PER_PAGE}`
      }
      
      return conn.query(query, [ userId, userId ], function (err, rows, fields) {
      if (err) throw err;
      res.json(rows)
    })
  }
  catch (err) {
    console.log(err)
    res.json({ error: true, message: err.message })
  }
})

module.exports = router;
