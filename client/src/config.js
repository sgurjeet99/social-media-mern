const BASE_URL = 'http://localhost:8001'

const FETCH_USERS = BASE_URL + '/users'
const FETCH_USER_FRIENDS = BASE_URL + '/friends'
const FETCH_USER_FOF = BASE_URL + '/fof'

export default {
  FETCH_USERS,
  FETCH_USER_FRIENDS,
  FETCH_USER_FOF
}