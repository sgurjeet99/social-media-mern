import React, { Component } from 'react'
import './home.css'
import { fetchFriends, fetchFriendsOfFriends, fetchUsers } from '../ServiceClass'
import UserList from './UserList'

export default class Homepage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      users: [],
      friends: {},
      fof: {},
      userPage: 0,
      friendPage: 0,
      fofPage: 0,
      isLoading: true
    }
  }

  componentDidMount() {
    this.showUsers()
  }

  showUsers = (userPage = this.state.userPage) => {
    fetchUsers(userPage)
    .then(data => {
      this.setState({
        users: data,
        isLoading: false
      })
    })
    .catch(err => {
      this.setState({
        users: [],
        isLoading: false
      })
    })
  }

  showFriends = (user, friendPage = this.state.friendPage) => {
    this.setState({
      selectedUser: user,
      selectedFriend: null
    })

    // if(this.state.friends[userId]) return

    this.setState({
      isLoadingFriends: true
    })

    fetchFriends(user.id, friendPage)
    .then(data => {
      let friends = { ...this.state.friends, [user.id]: data }
      this.setState({
        friends,
        isLoadingFriends: false
      })
    })
    .catch(err => {
      this.setState({
        isLoadingFriends: false,
        selectedUser: null
      })
    })
  }

  showFoF = (user, fofPage = this.state.fofPage) => {
    this.setState({
      selectedFriend: user,
      selectedUser: null
    })

    // if(this.state.fof[userId]) return

    this.setState({
      isLoadingFoF: true
    })

    fetchFriendsOfFriends(user.id, fofPage)
    .then(data => {
      let fof = { ...this.state.fof, [user.id]: data }
      this.setState({
        fof,
        isLoadingFoF: false
      })
    })
    .catch(err => {
      this.setState({
        isLoadingFoF: false,
        selectedFriend: null
      })
    })
  }

  handlePageChange = (source, offset) => {
    let { userPage, friendPage, fofPage } = this.state
    switch (source) {
      case 'user':
        userPage += offset
        this.showUsers(userPage)
        break;
      case 'friend':
        friendPage += offset
        this.showFriends(this.state.selectedUser, friendPage)
        break;
      case 'fof':
        fofPage += offset
        this.showFoF(this.state.selectedFriend, fofPage)
        break;
      default:
        break;
    }
    this.setState({
      userPage,
      friendPage,
      fofPage
    })
  }

  render() {
    return (
      <>
        {this.state.isLoading ? <div style={{ padding: 8 }}>Loading...</div> : (
          <div style={{ padding: 8 }}>
            <UserList data={this.state.users} page={this.state.userPage} handlePageChange={(offset) => this.handlePageChange('user', offset)} showFriends={this.showFriends} showFof={this.showFoF} class={'userList'} />
            <hr/>
            {this.state.selectedUser && (
              this.state.isLoadingFriends ? <div>Loading {this.state.selectedUser.fname}'s friends...</div> : (
                <div>
                  Showing {this.state.selectedUser.fname}'s friends:
                  <UserList data={this.state.friends[this.state.selectedUser.id]} page={this.state.friendPage} handlePageChange={(offset) => this.handlePageChange('friend', offset)} class={'friendList'} />
                </div>
              )
            )}
            {this.state.selectedFriend && (
              this.state.isLoadingFof ? <div>Loading {this.state.selectedFof.fname}'s friends...</div> : (
                <div>
                  Showing {this.state.selectedFriend.fname}'s friends of friends:
                  <UserList data={this.state.fof[this.state.selectedFriend.id]} page={this.state.fofPage} handlePageChange={(offset) => this.handlePageChange('fof', offset)} class={'fofList'} />
                </div>
              )
            )}
          </div>
        )}
      </>
    )
  }
}
