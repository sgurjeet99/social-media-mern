import React from 'react'
import './home.css'

export default function UserList(props) {
  return (
    <div>
      {(!props.data || props.data.length === 0) ? <div style={{ margin: 8 }}>No more results...</div> : (
        <ul>
          {props.data.map(user => {
            return (
              <li
                className={props.class}
                key={user.id}
                onClick={() => props.showChildren && props.showChildren(user)}
              >
                <span>
                  {user.fname} {user.lname}
                  <span>
                    {props.showFriends && <button
                      onClick={() => props.showFriends(user)}
                    >
                      Friends
                    </button>
                  }
                  </span>
                  <span>
                  {props.showFof && <button
                      onClick={() => props.showFof(user)}
                    >
                      Friends of friends
                    </button>
                  }
                  </span>
                </span>
              </li>
            )
          })}
        </ul>
      )}
      <div>
        <span>
          <button
            onClick={() => props.handlePageChange(-1)}
            disabled={props.page === 0}
          >
            Back
          </button>
        </span>
        Page {props.page + 1}
        <span>
          <button
            onClick={() => props.handlePageChange(1)}
            disabled={props.data && props.data.length === 0}
          >
            Next
          </button>
        </span>
      </div>
    </div>
  )
}
