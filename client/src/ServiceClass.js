import CONFIG from './config'

export const fetchUsers = (page = 0) => {
  return fetch(CONFIG.FETCH_USERS + `?page=${page}`)
  .then(res => res.json())
}

export const fetchFriends = (userId, page = 0) => {
  return fetch(CONFIG.FETCH_USER_FRIENDS + `?userId=${userId}&page=${page}`)
  .then(res => res.json())
}

export const fetchFriendsOfFriends = (userId, page = 0) => {
  return fetch(CONFIG.FETCH_USER_FOF + `?userId=${userId}&page=${page}`)
  .then(res => res.json())
}