# Simple MERN stack social media application

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# React Social Media SPA

An example SPA using ReactJS for a simple social media website with users and friends' relationships.

## Development 

- Clone the repo:

```bash
$ git clone https://sgurjeet99@bitbucket.org/sgurjeet99/social-media-mock.git
```

- Go to the project directory and install dependencies:

```bash
$ cd social-media-mock/client && npm install
```

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

#### What you need to run this code
1. Node (>11.9.0)
2. NPM (>6.5.0)

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

# Express web server using MySQL

#### Database schema migration
We can use db-migrate to migrate existing MySQL DB to the new schema.

### Database Schemas/DDL


```create table user
(
	id int auto_increment primary key,
	fname varchar(48) null,
	lname varchar(48) null,
	constraint user_id_index
		unique (id)
);```

```create table userFriends
(
	user_id int not null,
	friend_id int not null,
	primary key (user_id, friend_id),
	constraint userFriends_user_id_fk
		foreign key (user_id) references user (id)
			on update cascade on delete cascade,
	constraint userFriends_friend_id_fk
		foreign key (friend_id) references user (id)
			on update cascade on delete cascade
);```

#### Adding sample user info and relationships

```insert into user(fname, lname)
values
    ('Gurjeet','Babbar'),
    ('Jimmy','Hendrix'),
    ('Mike','Tyson'),
    ('Dwayne','Johnson'),
    ('Steve','Smith'),
    ('Robert','Downey'),
    ('Karl','Urban'),
    ('Bill', 'Gates');```

```insert into userFriends
values
    (1,2),
    (1,3),
    (2,4),
    (2,8),
    (3,7),
    (6,2),
    (6,5),
    (2,1),
    (3,1),
    (4,2),
    (8,2),
    (7,3),
    (2,6),
    (5,6);```

## Build and Test

#### What you need to run this code
1. Node (>11.9.0)
2. NPM (>6.5.0)
3. mysql (8.0.21)

### Getting Started
In the project directory, you can run:

```bash
$ cd social-media-mock/server
```

```bash
$ npm install
```

Add MySQL credentials in config file.

### Available scripts 

#### `npm start`
Runs the express web server in the development mode at [http://localhost:8001](http://localhost:8001).

### Testing endpoints(Postman/Insomnia)

#### Fetch users (/users)
Look up all users.
METHOD: GET

Available query params:
page - number (By default, every page has 3 records)

Sample requests:
```
/users
/users?page=2
```

#### Fetch friends of a user (/friends)
Get all friends of a user
METHOD: GET

Available query params:
userId - number (required)
page - number (optional; By default, every page has 3 records)

Sample requests:
```
/friends?userId=2
/friends?userId=1&page=2
```

#### Fetch friends of friends of a user (/fof)
Get all friends of a user
METHOD: GET

Available query params:
userId - number (required)
page - number (optional; By default, every page has 3 records)

Sample requests:
```
/fof?userId=2
/fof?userId=1&page=2
```


## Testing

### React SPA

We can use jest and react-testing-library to write unit or integration tests.
Fetch requests can be made and equated with the expected result.

### Node express server

We can use chai and chai-http to make requests to our API endpoints.
We can check the request against the expected result.